#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/if_ether.h>
#include <linux/inet.h>

typedef __u8 byte;
typedef __u16 word;
typedef __u32 double_word, dw;
typedef enum {m_true=1,m_false=0} m_bool;

static struct nf_hook_ops hook1; 

#define rst_list_size 10
dw rst_list[rst_list_size]={0},rst_top=0;
void rst_rec(dw ip){
   rst_list[rst_top]=ip;
}
void rst_roll(void){
   if(++rst_top==rst_list_size){rst_top=0;}
   rst_list[rst_top]=0;
}
int rst_check(dw ip){
   //检查除了top以外的元素
   dw i=rst_top+1;
   for(; i<rst_list_size; i++){
      if(rst_list[i]==ip){
         rst_list[i]=0;
         return rst_list_size+rst_top-i;
      }
   }
   i=0;
   for(; i<rst_top; i++){
      if(rst_list[i]==ip){
         rst_list[i]=0;
         return rst_top-i;
      }
   }
   //检查top元素
   if(rst_list[rst_top]==ip){
      rst_list[rst_top]=0;
      return rst_list_size;
   }
   return 0;
}

#define syn_alert 20
dw syn_counter_1=0,syn_counter_2=0;
void syn_count(void){syn_counter_1++;}
//m_true为遭到flood攻击
m_bool syn_check(void){
   //二阶计数器迭代
   switch(syn_counter_1){
      case 0:syn_counter_2+=10;break;
      case 1:syn_counter_2+=5;break;
      case 2:syn_counter_2+=3;break;
      case 3:syn_counter_2+=1;break;
      case 4:break;
      default:syn_counter_2/=2;
   }
   syn_counter_1=0;
   //二阶计数器检验
   if(syn_counter_2>=syn_alert){
      syn_counter_2=0;
      return m_true;
   }
   return m_false;
}

unsigned int watcher(void *priv, struct sk_buff *skb,
                       const struct nf_hook_state *state)
{
   struct iphdr *iph;
   struct tcphdr *tcph;
   iph = ip_hdr(skb);
   if(iph->protocol!=IPPROTO_TCP){return NF_ACCEPT;}
   tcph = tcp_hdr(skb);


   //RST逻辑
   if(tcph->rst){
   //RST包
      if(rst_check(iph->saddr)){
         printk(KERN_WARNING "发现来自 %pI4 的连续RST数据包，您可能遇到了RST攻击。\r\n", &(iph->daddr));         
      }
      rst_rec(iph->saddr);
      rst_roll();
   }
   //其他包
   else if(tcph->ack==1&&tcph->syn==0){
      int check=rst_check(iph->saddr);
      if(check){
         if(check<5){
            printk(KERN_WARNING "发现来自 %pI4 的RST数据包，您可能受到RST攻击。\r\n", &(iph->daddr));
         }
         else{
            printk(KERN_WARNING "发现来自 %pI4 的RST数据包，您的TCP连接可能异常中断。\r\n", &(iph->daddr));
         }
      }
      rst_roll();
   }

   //SYN_Flood
   if(tcph->syn==1&&tcph->ack==0){
   //同步包逻辑
      if(syn_check()){
         printk(KERN_WARNING "您很可能遭到同步泛洪攻击，请注意开启SYN_COOKIE，并做好其他防护。\r\n", &(iph->daddr));
      }
   }
   //其他逻辑
   else{
      syn_count();
   }


   return NF_ACCEPT;
}

int registerFilter(void) {
   printk(KERN_INFO "正在安装模块。\n");

   hook1.hook = watcher;
   hook1.hooknum = NF_INET_LOCAL_IN;
   hook1.pf = PF_INET;
   hook1.priority = NF_IP_PRI_FIRST;
   nf_register_net_hook(&init_net, &hook1);

   return 0;
}

void removeFilter(void) {
   printk(KERN_INFO "模块已卸载。\r\n");
   nf_unregister_net_hook(&init_net, &hook1);
   nf_unregister_net_hook(&init_net, &hook1);
}

module_init(registerFilter);
module_exit(removeFilter);

MODULE_LICENSE("GPL");

