obj-m += TCP-Filter.o
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

ins:
	sudo dmesg -C
	sudo insmod TCP-Filter.ko

rm:
	sudo rmmod TCP-Filter

